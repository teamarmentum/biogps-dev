import { BiogpsPage } from './app.po';

describe('biogps App', function() {
  let page: BiogpsPage;

  beforeEach(() => {
    page = new BiogpsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
