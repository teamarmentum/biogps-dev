$(function () {
    
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}
    var options = {
        vertical_margin: 10,
        horizontal_margin: 0,
        resizable: {
        handles: 'e, se,nw,n,ne, s, sw, w'
    },
        alwaysShowResizeHandle: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
        
    };
    $('.grid-stack').gridstack(options);
    
    new function () {
        
        var ds_widget_box_size_manager=getCookie("ds_widget_box_size_manager");
        if(ds_widget_box_size_manager){
            
            
            var ds_widget_box_size_manager_json = JSON.parse(ds_widget_box_size_manager);
            this.serializedData = ds_widget_box_size_manager_json;
            
        }else{
            this.serializedData = [
                    {id:'ds_widget_box-1',x: 0, y: 0, width: 6, height: 5},
                    {id:'ds_widget_box-2',x: 6, y: 0, width: 6, height:5 },
                    {id:'ds_widget_box-3',x: 0, y: 0, width: 6, height: 5},
                    {id:'ds_widget_box-4',x: 6, y: 0, width: 6, height:5 },
                ];
            
        }
                

                this.grid = $('.grid-stack').data('gridstack');

                this.loadGrid = function () {
                    var this_widget = this;
                    this.grid.removeAll();
                    $('#drag_and_save').hide();
                    var items = GridStackUI.Utils.sort(this.serializedData);
                    _.each(items, function (node) {
                        console.log(node);
                        var nit = $('#drag_and_save #'+node.id);
                        var nita= '<div class="grid-stack-item col-sm-6 zoom-box-container   ds_draggable_item" data-gs-x="0" data-gs-y="0"  data-gs-width="4" data-gs-height="2" id="'+node.id+'"><div class="grid-stack-item-content zoom-box-style">'+nit.html()+'</div></div>';
                        var asd= this.grid.addWidget(nita,
                            node.x, node.y, node.width, node.height);
                    }, this);


 $(document.body).on('click','.remove-box,.pe-7s-close',function(){
                        var th = jQuery(this).parents('.grid-stack-item');
            	      var asd= this_widget.grid.removeWidget(th);
            	 });
                    
           var current_element= this;         
    $('.grid-stack-item').mouseup(function(){
     setTimeout(function(){
          current_element.saveGrid();
     },10)
    });
                    return false;
                }.bind(this);

                this.saveGrid = function () {
                    this.serializedData = _.map($('.grid-stack > .grid-stack-item:visible'), function (el) {
                        el = $(el);
                        var node = el.data('_gridstack_node');
                        var updated_order = {
                            id: el.attr('id'),
                            x: node.x,
                            y: node.y,
                            width: node.width,
                            height: node.height
                        };
                        return updated_order;
                    }, this);
                     setCookie("ds_widget_box_size_manager", JSON.stringify(this.serializedData), 365);
                    $('#saved-data').val(JSON.stringify(this.serializedData, null, '    '));
                     $('#save-grid').html('Saved');
                     setTimeout(function(){
                          $('#save-grid').html('Save changes');
                     },1000)
                    return false;
                }.bind(this);

                this.clearGrid = function () {
                    this.grid.removeAll();
                    return false;
                }.bind(this);

                $('#save-grid').click(this.saveGrid);
                $('#load-grid').click(this.loadGrid);
                $('#clear-grid').click(this.clearGrid);

                this.loadGrid();
            };

jQuery('.Dashboard-tab-part .nav-tabs li a').click(function(){
                var id = jQuery(this).attr('href');
                var id_2_class = id.replace('#','');
                jQuery('body').attr('data-active-tab-cgl',id_2_class);
            });
});

