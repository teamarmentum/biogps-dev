import { InMemoryDbService } from 'angular2-in-memory-web-api';
import { Tweet } from './tweet';

export class MockDatabaseService implements InMemoryDbService {
    createDb() {
        let friends = ["Vincent", "Charlie", "Jaz", "John"];

        let tweets = [
            new Tweet(1, 'Test This is my lenged tweet by Charlie', 'Charlie', new Date(), ['Jaz'], []),
            new Tweet(2, 'This is my lenged tweet by Jaz', 'Jaz', new Date(), ['Charlie'], ['Vincent']),
            new Tweet(3, 'This is my lenged tweet by John', 'John', new Date(), ['Charlie', 'Vincent'], ['Charlie', 'Vincent']),
            new Tweet(4, 'This is my lenged tweet by Vincent', 'Vincent', new Date(), ['Vincent'], ['Charlie', 'Vincent']),
            ];
        let data = {
    "data": {
        "geneList": [
            {
                "_id": "4331",
                "_score": 17.735683,
                "entrezgene": 4331,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            4331
                        ],
                        [
                            10090,
                            17420
                        ],
                        [
                            10116,
                            266713
                        ],
                        [
                            7227,
                            36130
                        ],
                        [
                            6239,
                            173602
                        ],
                        [
                            7955,
                            450080
                        ],
                        [
                            8364,
                            549892
                        ]
                    ],
                    "id": 1821
                },
                "id": "4331",
                "name": "MNAT1, CDK activating kinase assembly factor",
                "symbol": "MNAT1",
                "taxid": 9606
            },
            {
                "_id": "817341",
                "_score": 17.229946,
                "entrezgene": 817341,
                "homologene": {
                    "genes": [
                        [
                            10090,
                            624855
                        ],
                        [
                            10116,
                            681162
                        ],
                        [
                            7227,
                            34250
                        ],
                        [
                            3702,
                            817340
                        ],
                        [
                            3702,
                            817341
                        ]
                    ],
                    "id": 83385
                },
                "id": "817341",
                "name": "CDK-subunit 2",
                "symbol": "CKS2",
                "taxid": 3702
            },
            {
                "_id": "829019",
                "_score": 16.627909,
                "entrezgene": 829019,
                "homologene": {
                    "genes": [
                        [
                            6239,
                            186662
                        ],
                        [
                            3702,
                            829019
                        ]
                    ],
                    "id": 96591
                },
                "id": "829019",
                "name": "CDK-activating kinase 1AT",
                "symbol": "CAK1AT",
                "taxid": 3702
            },
            {
                "_id": "388324",
                "_score": 16.368792,
                "entrezgene": 388324,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            388324
                        ],
                        [
                            10090,
                            103844
                        ],
                        [
                            10116,
                            360555
                        ]
                    ],
                    "id": 19481
                },
                "id": "388324",
                "name": "inhibitor of CDK, cyclin A1 interacting protein 1",
                "symbol": "INCA1",
                "taxid": 9606
            },
            {
                "_id": "842993",
                "_score": 15.840766,
                "entrezgene": 842993,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1022
                        ],
                        [
                            10090,
                            12572
                        ],
                        [
                            10116,
                            171150
                        ],
                        [
                            7227,
                            31441
                        ],
                        [
                            6239,
                            171784
                        ],
                        [
                            7955,
                            405897
                        ],
                        [
                            3702,
                            842993
                        ],
                        [
                            8364,
                            549973
                        ]
                    ],
                    "id": 1363
                },
                "id": "842993",
                "name": "CDK-activating kinase 4",
                "symbol": "CAK4",
                "taxid": 3702
            },
            {
                "_id": "831702",
                "_score": 13.197513,
                "entrezgene": 831702,
                "id": "831702",
                "name": "CDK inhibitor P21 binding protein",
                "symbol": "AT5G03830",
                "taxid": 3702
            },
            {
                "_id": "819058",
                "_score": 13.197513,
                "entrezgene": 819058,
                "homologene": {
                    "genes": [
                        [
                            3702,
                            819058
                        ]
                    ],
                    "id": 135202
                },
                "id": "819058",
                "name": "CDK inhibitor P21 binding protein",
                "symbol": "AT2G44510",
                "taxid": 3702
            },
            {
                "_id": "ENSSSCG00000023440",
                "_score": 13.197513,
                "id": "ENSSSCG00000023440",
                "name": "CDK-activating kinase assembly factor MAT1",
                "taxid": 9823
            },
            {
                "_id": "103844",
                "_score": 12.799388,
                "entrezgene": 103844,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            388324
                        ],
                        [
                            10090,
                            103844
                        ],
                        [
                            10116,
                            360555
                        ]
                    ],
                    "id": 19481
                },
                "id": "103844",
                "name": "inhibitor of CDK, cyclin A1 interacting protein 1",
                "symbol": "Inca1",
                "taxid": 10090
            },
            {
                "_id": "100738515",
                "_score": 12.307104,
                "entrezgene": 100738515,
                "id": "100738515",
                "name": "CDK-activating kinase assembly factor MAT1",
                "symbol": "LOC100738515",
                "taxid": 9823
            },
            {
                "_id": "ENSSSCG00000005090",
                "_score": 12.307104,
                "id": "ENSSSCG00000005090",
                "name": "MNAT1, CDK activating kinase assembly factor",
                "symbol": "MNAT1",
                "taxid": 9823
            },
            {
                "_id": "266713",
                "_score": 10.069292,
                "entrezgene": 266713,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            4331
                        ],
                        [
                            10090,
                            17420
                        ],
                        [
                            10116,
                            266713
                        ],
                        [
                            7227,
                            36130
                        ],
                        [
                            6239,
                            173602
                        ],
                        [
                            7955,
                            450080
                        ],
                        [
                            8364,
                            549892
                        ]
                    ],
                    "id": 1821
                },
                "id": "266713",
                "name": "MNAT CDK-activating kinase assembly factor 1",
                "symbol": "Mnat1",
                "taxid": 10116
            },
            {
                "_id": "360555",
                "_score": 10.069292,
                "entrezgene": 360555,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            388324
                        ],
                        [
                            10090,
                            103844
                        ],
                        [
                            10116,
                            360555
                        ]
                    ],
                    "id": 19481
                },
                "id": "360555",
                "name": "inhibitor of CDK, cyclin A1 interacting protein 1",
                "symbol": "Inca1",
                "taxid": 10116
            },
            {
                "_id": "ENSXETG00000016938",
                "_score": 9.845683,
                "id": "ENSXETG00000016938",
                "name": "MNAT CDK-activating kinase assembly factor 1",
                "symbol": "mnat1",
                "taxid": 8364
            },
            {
                "_id": "450080",
                "_score": 9.153902,
                "entrezgene": 450080,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            4331
                        ],
                        [
                            10090,
                            17420
                        ],
                        [
                            10116,
                            266713
                        ],
                        [
                            7227,
                            36130
                        ],
                        [
                            6239,
                            173602
                        ],
                        [
                            7955,
                            450080
                        ],
                        [
                            8364,
                            549892
                        ]
                    ],
                    "id": 1821
                },
                "id": "450080",
                "name": "MNAT CDK-activating kinase assembly factor 1",
                "symbol": "mnat1",
                "taxid": 7955
            },
            {
                "_id": "100511224",
                "_score": 9.153902,
                "entrezgene": 100511224,
                "id": "100511224",
                "name": "inhibitor of CDK, cyclin A1 interacting protein 1",
                "symbol": "INCA1",
                "taxid": 9823
            },
            {
                "_id": "ENSSSCG00000017901",
                "_score": 9.153902,
                "id": "ENSSSCG00000017901",
                "name": "inhibitor of CDK, cyclin A1 interacting protein 1",
                "symbol": "INCA1",
                "taxid": 9823
            },
            {
                "_id": "549892",
                "_score": 9.153902,
                "entrezgene": 549892,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            4331
                        ],
                        [
                            10090,
                            17420
                        ],
                        [
                            10116,
                            266713
                        ],
                        [
                            7227,
                            36130
                        ],
                        [
                            6239,
                            173602
                        ],
                        [
                            7955,
                            450080
                        ],
                        [
                            8364,
                            549892
                        ]
                    ],
                    "id": 1821
                },
                "id": "549892",
                "name": "MNAT CDK-activating kinase assembly factor 1",
                "symbol": "mnat1",
                "taxid": 8364
            },
            {
                "_id": "829205",
                "_score": 7.920383,
                "entrezgene": 829205,
                "homologene": {
                    "genes": [
                        [
                            3702,
                            829205
                        ]
                    ],
                    "id": 96401
                },
                "id": "829205",
                "name": "cyclin-dependent kinase-activating kinase assembly factor-related / CDK-activating kinase assembly factor-like protein",
                "symbol": "AT4G30820",
                "taxid": 3702
            },
            {
                "_id": "181247",
                "_score": 5.750021,
                "entrezgene": 181247,
                "homologene": {
                    "genes": [
                        [
                            6239,
                            174244
                        ],
                        [
                            6239,
                            181247
                        ]
                    ],
                    "id": 137737
                },
                "id": "181247",
                "name": "Cyclin-Dependent Kinase family",
                "symbol": "cdk-11.2",
                "taxid": 6239
            },
            {
                "_id": "174244",
                "_score": 5.0831885,
                "entrezgene": 174244,
                "homologene": {
                    "genes": [
                        [
                            6239,
                            174244
                        ],
                        [
                            6239,
                            181247
                        ]
                    ],
                    "id": 137737
                },
                "id": "174244",
                "name": "Putative serine/threonine-protein kinase B0495.2",
                "symbol": "cdk-11.1",
                "taxid": 6239
            },
            {
                "_id": "172677",
                "_score": 4.4445763,
                "entrezgene": 172677,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1024
                        ],
                        [
                            10090,
                            264064
                        ],
                        [
                            10116,
                            498140
                        ],
                        [
                            7227,
                            39157
                        ],
                        [
                            6239,
                            172677
                        ],
                        [
                            7955,
                            373868
                        ],
                        [
                            8364,
                            394856
                        ]
                    ],
                    "id": 55565
                },
                "id": "172677",
                "name": "Cyclin-dependent kinase 8",
                "symbol": "cdk-8",
                "taxid": 6239
            },
            {
                "_id": "171784",
                "_score": 4.396945,
                "entrezgene": 171784,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1022
                        ],
                        [
                            10090,
                            12572
                        ],
                        [
                            10116,
                            171150
                        ],
                        [
                            7227,
                            31441
                        ],
                        [
                            6239,
                            171784
                        ],
                        [
                            7955,
                            405897
                        ],
                        [
                            3702,
                            842993
                        ],
                        [
                            8364,
                            549973
                        ]
                    ],
                    "id": 1363
                },
                "id": "171784",
                "name": "Cyclin-dependent kinase 7",
                "symbol": "cdk-7",
                "taxid": 6239
            },
            {
                "_id": "173023",
                "_score": 4.025015,
                "entrezgene": 173023,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1025
                        ],
                        [
                            10090,
                            107951
                        ],
                        [
                            10116,
                            362110
                        ],
                        [
                            7227,
                            37586
                        ],
                        [
                            6239,
                            173023
                        ],
                        [
                            7955,
                            321602
                        ],
                        [
                            8364,
                            448039
                        ]
                    ],
                    "id": 55566
                },
                "id": "173023",
                "name": "Probable cyclin-dependent kinase 9",
                "symbol": "cdk-9",
                "taxid": 6239
            },
            {
                "_id": "175559",
                "_score": 3.9027188,
                "entrezgene": 175559,
                "homologene": {
                    "genes": [
                        [
                            6239,
                            175559
                        ],
                        [
                            3702,
                            830891
                        ],
                        [
                            3702,
                            836620
                        ]
                    ],
                    "id": 134926
                },
                "id": "175559",
                "name": "Cyclin-dependent kinase 12",
                "symbol": "cdk-12",
                "taxid": 6239
            },
            {
                "_id": "176774",
                "_score": 3.8096368,
                "entrezgene": 176774,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1020
                        ],
                        [
                            10090,
                            12568
                        ],
                        [
                            10116,
                            140908
                        ],
                        [
                            7227,
                            36727
                        ],
                        [
                            6239,
                            176774
                        ],
                        [
                            7955,
                            65234
                        ],
                        [
                            8364,
                            780236
                        ]
                    ],
                    "id": 3623
                },
                "id": "176774",
                "name": "Cyclin-dependent-like kinase 5",
                "symbol": "cdk-5",
                "taxid": 6239
            },
            {
                "_id": "171911",
                "_score": 3.8096368,
                "entrezgene": 171911,
                "id": "171911",
                "name": "Cyclin-dependent kinase 2",
                "symbol": "cdk-2",
                "taxid": 6239
            },
            {
                "_id": "181472",
                "_score": 3.7688103,
                "entrezgene": 181472,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1021
                        ],
                        [
                            10090,
                            12571
                        ],
                        [
                            10116,
                            114483
                        ],
                        [
                            7227,
                            36854
                        ],
                        [
                            6239,
                            181472
                        ],
                        [
                            7955,
                            100034507
                        ],
                        [
                            8364,
                            100489734
                        ]
                    ],
                    "id": 963
                },
                "id": "181472",
                "name": "Cyclin-dependent kinase 4 homolog",
                "symbol": "cdk-4",
                "taxid": 6239
            },
            {
                "_id": "176374",
                "_score": 2.8750105,
                "entrezgene": 176374,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            983
                        ],
                        [
                            10090,
                            12534
                        ],
                        [
                            10116,
                            54237
                        ],
                        [
                            7227,
                            34411
                        ],
                        [
                            6239,
                            176374
                        ],
                        [
                            7955,
                            80973
                        ],
                        [
                            8364,
                            394503
                        ]
                    ],
                    "id": 68203
                },
                "id": "176374",
                "name": "Cyclin-dependent kinase 1",
                "symbol": "cdk-1",
                "taxid": 6239
            },
            {
                "_id": "549973",
                "_score": 1.4213073,
                "entrezgene": 549973,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1022
                        ],
                        [
                            10090,
                            12572
                        ],
                        [
                            10116,
                            171150
                        ],
                        [
                            7227,
                            31441
                        ],
                        [
                            6239,
                            171784
                        ],
                        [
                            7955,
                            405897
                        ],
                        [
                            3702,
                            842993
                        ],
                        [
                            8364,
                            549973
                        ]
                    ],
                    "id": 1363
                },
                "id": "549973",
                "name": "cyclin-dependent kinase 7",
                "symbol": "cdk7",
                "taxid": 8364
            },
            {
                "_id": "100310799",
                "_score": 1.2698789,
                "entrezgene": 100310799,
                "id": "100310799",
                "name": "menage a trois homolog 1, cyclin H assembly factor (Xenopus laevis)",
                "symbol": "MNAT1",
                "taxid": 9823
            },
            {
                "_id": "105278",
                "_score": 1.0571284,
                "entrezgene": 105278,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            23552
                        ],
                        [
                            10090,
                            105278
                        ],
                        [
                            10116,
                            364666
                        ],
                        [
                            7227,
                            42562
                        ],
                        [
                            7955,
                            494112
                        ]
                    ],
                    "id": 8109
                },
                "id": "105278",
                "name": "cyclin-dependent kinase 20",
                "symbol": "Cdk20",
                "taxid": 10090
            },
            {
                "_id": "1030",
                "_score": 0.9836988,
                "entrezgene": 1030,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1030
                        ],
                        [
                            10090,
                            12579
                        ],
                        [
                            10116,
                            25164
                        ]
                    ],
                    "id": 55859
                },
                "id": "1030",
                "name": "cyclin dependent kinase inhibitor 2B",
                "symbol": "CDKN2B",
                "taxid": 9606
            },
            {
                "_id": "143384",
                "_score": 0.9736093,
                "entrezgene": 143384,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            143384
                        ],
                        [
                            10090,
                            78832
                        ],
                        [
                            10116,
                            365493
                        ],
                        [
                            7955,
                            100538081
                        ],
                        [
                            8364,
                            100497029
                        ]
                    ],
                    "id": 17821
                },
                "id": "143384",
                "name": "CDK2 associated cullin domain 1",
                "symbol": "CACUL1",
                "taxid": 9606
            },
            {
                "_id": "171150",
                "_score": 0.9574064,
                "entrezgene": 171150,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1022
                        ],
                        [
                            10090,
                            12572
                        ],
                        [
                            10116,
                            171150
                        ],
                        [
                            7227,
                            31441
                        ],
                        [
                            6239,
                            171784
                        ],
                        [
                            7955,
                            405897
                        ],
                        [
                            3702,
                            842993
                        ],
                        [
                            8364,
                            549973
                        ]
                    ],
                    "id": 1363
                },
                "id": "171150",
                "name": "cyclin-dependent kinase 7",
                "symbol": "Cdk7",
                "taxid": 10116
            },
            {
                "_id": "12572",
                "_score": 0.9345328,
                "entrezgene": 12572,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1022
                        ],
                        [
                            10090,
                            12572
                        ],
                        [
                            10116,
                            171150
                        ],
                        [
                            7227,
                            31441
                        ],
                        [
                            6239,
                            171784
                        ],
                        [
                            7955,
                            405897
                        ],
                        [
                            3702,
                            842993
                        ],
                        [
                            8364,
                            549973
                        ]
                    ],
                    "id": 1363
                },
                "id": "12572",
                "name": "cyclin-dependent kinase 7",
                "symbol": "Cdk7",
                "taxid": 10090
            },
            {
                "_id": "1022",
                "_score": 0.90465885,
                "entrezgene": 1022,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1022
                        ],
                        [
                            10090,
                            12572
                        ],
                        [
                            10116,
                            171150
                        ],
                        [
                            7227,
                            31441
                        ],
                        [
                            6239,
                            171784
                        ],
                        [
                            7955,
                            405897
                        ],
                        [
                            3702,
                            842993
                        ],
                        [
                            8364,
                            549973
                        ]
                    ],
                    "id": 1363
                },
                "id": "1022",
                "name": "cyclin dependent kinase 7",
                "symbol": "CDK7",
                "taxid": 9606
            },
            {
                "_id": "821449",
                "_score": 0.88955796,
                "entrezgene": 821449,
                "id": "821449",
                "name": "KIP-related protein 6",
                "symbol": "KRP6",
                "taxid": 3702
            },
            {
                "_id": "56647",
                "_score": 0.88229567,
                "entrezgene": 56647,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            56647
                        ],
                        [
                            10090,
                            66165
                        ],
                        [
                            10116,
                            361666
                        ],
                        [
                            7227,
                            41690
                        ],
                        [
                            7955,
                            541348
                        ],
                        [
                            8364,
                            448392
                        ]
                    ],
                    "id": 41629
                },
                "id": "56647",
                "name": "BRCA2 and CDKN1A interacting protein",
                "symbol": "BCCIP",
                "taxid": 9606
            },
            {
                "_id": "31441",
                "_score": 0.78054374,
                "entrezgene": 31441,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1022
                        ],
                        [
                            10090,
                            12572
                        ],
                        [
                            10116,
                            171150
                        ],
                        [
                            7227,
                            31441
                        ],
                        [
                            6239,
                            171784
                        ],
                        [
                            7955,
                            405897
                        ],
                        [
                            3702,
                            842993
                        ],
                        [
                            8364,
                            549973
                        ]
                    ],
                    "id": 1363
                },
                "id": "31441",
                "name": "Cyclin-dependent kinase 7",
                "symbol": "Cdk7",
                "taxid": 7227
            },
            {
                "_id": "17420",
                "_score": 0.7475027,
                "entrezgene": 17420,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            4331
                        ],
                        [
                            10090,
                            17420
                        ],
                        [
                            10116,
                            266713
                        ],
                        [
                            7227,
                            36130
                        ],
                        [
                            6239,
                            173602
                        ],
                        [
                            7955,
                            450080
                        ],
                        [
                            8364,
                            549892
                        ]
                    ],
                    "id": 1821
                },
                "id": "17420",
                "name": "menage a trois 1",
                "symbol": "Mnat1",
                "taxid": 10090
            },
            {
                "_id": "1026",
                "_score": 0.6959034,
                "entrezgene": 1026,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1026
                        ],
                        [
                            10090,
                            12575
                        ],
                        [
                            10116,
                            114851
                        ]
                    ],
                    "id": 333
                },
                "id": "1026",
                "name": "cyclin dependent kinase inhibitor 1A",
                "symbol": "CDKN1A",
                "taxid": 9606
            },
            {
                "_id": "23552",
                "_score": 0.6894074,
                "entrezgene": 23552,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            23552
                        ],
                        [
                            10090,
                            105278
                        ],
                        [
                            10116,
                            364666
                        ],
                        [
                            7227,
                            42562
                        ],
                        [
                            7955,
                            494112
                        ]
                    ],
                    "id": 8109
                },
                "id": "23552",
                "name": "cyclin dependent kinase 20",
                "symbol": "CDK20",
                "taxid": 9606
            },
            {
                "_id": "902",
                "_score": 0.68184334,
                "entrezgene": 902,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            902
                        ],
                        [
                            10090,
                            66671
                        ],
                        [
                            10116,
                            84389
                        ],
                        [
                            7227,
                            40429
                        ],
                        [
                            6239,
                            190078
                        ],
                        [
                            7955,
                            541325
                        ],
                        [
                            3702,
                            832824
                        ],
                        [
                            8364,
                            549010
                        ]
                    ],
                    "id": 946
                },
                "id": "902",
                "name": "cyclin H",
                "symbol": "CCNH",
                "taxid": 9606
            },
            {
                "_id": "1032",
                "_score": 0.66172177,
                "entrezgene": 1032,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1032
                        ],
                        [
                            10090,
                            12581
                        ],
                        [
                            7955,
                            100333252
                        ],
                        [
                            8364,
                            780300
                        ]
                    ],
                    "id": 36081
                },
                "id": "1032",
                "name": "cyclin dependent kinase inhibitor 2D",
                "symbol": "CDKN2D",
                "taxid": 9606
            },
            {
                "_id": "1033",
                "_score": 0.6238772,
                "entrezgene": 1033,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1033
                        ],
                        [
                            10090,
                            72391
                        ],
                        [
                            7955,
                            402796
                        ]
                    ],
                    "id": 3805
                },
                "id": "1033",
                "name": "cyclin dependent kinase inhibitor 3",
                "symbol": "CDKN3",
                "taxid": 9606
            },
            {
                "_id": "8812",
                "_score": 0.55712533,
                "entrezgene": 8812,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            8812
                        ],
                        [
                            10090,
                            12454
                        ],
                        [
                            10116,
                            500715
                        ],
                        [
                            7955,
                            569432
                        ],
                        [
                            8364,
                            779776
                        ]
                    ],
                    "id": 14748
                },
                "id": "8812",
                "name": "cyclin K",
                "symbol": "CCNK",
                "taxid": 9606
            },
            {
                "_id": "1021",
                "_score": 0.51175225,
                "entrezgene": 1021,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1021
                        ],
                        [
                            10090,
                            12571
                        ],
                        [
                            10116,
                            114483
                        ],
                        [
                            7227,
                            36854
                        ],
                        [
                            6239,
                            181472
                        ],
                        [
                            7955,
                            100034507
                        ],
                        [
                            8364,
                            100489734
                        ]
                    ],
                    "id": 963
                },
                "id": "1021",
                "name": "cyclin dependent kinase 6",
                "symbol": "CDK6",
                "taxid": 9606
            },
            {
                "_id": "9134",
                "_score": 0.49903467,
                "entrezgene": 9134,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            9134
                        ],
                        [
                            10090,
                            12448
                        ],
                        [
                            10116,
                            362485
                        ],
                        [
                            7955,
                            415165
                        ],
                        [
                            8364,
                            549021
                        ]
                    ],
                    "id": 7660
                },
                "id": "9134",
                "name": "cyclin E2",
                "symbol": "CCNE2",
                "taxid": 9606
            },
            {
                "_id": "12575",
                "_score": 0.49561083,
                "entrezgene": 12575,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1026
                        ],
                        [
                            10090,
                            12575
                        ],
                        [
                            10116,
                            114851
                        ]
                    ],
                    "id": 333
                },
                "id": "12575",
                "name": "cyclin-dependent kinase inhibitor 1A (P21)",
                "symbol": "Cdkn1a",
                "taxid": 10090
            },
            {
                "_id": "1027",
                "_score": 0.481912,
                "entrezgene": 1027,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1027
                        ],
                        [
                            10090,
                            12576
                        ],
                        [
                            10116,
                            83571
                        ],
                        [
                            7955,
                            368329
                        ]
                    ],
                    "id": 2999
                },
                "id": "1027",
                "name": "cyclin dependent kinase inhibitor 1B",
                "symbol": "CDKN1B",
                "taxid": 9606
            },
            {
                "_id": "234854",
                "_score": 0.4672664,
                "entrezgene": 234854,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            8558
                        ],
                        [
                            10090,
                            234854
                        ],
                        [
                            10116,
                            361434
                        ],
                        [
                            7227,
                            36051
                        ],
                        [
                            7955,
                            550285
                        ],
                        [
                            8364,
                            549329
                        ]
                    ],
                    "id": 55769
                },
                "id": "234854",
                "name": "cyclin-dependent kinase 10",
                "symbol": "Cdk10",
                "taxid": 10090
            },
            {
                "_id": "34411",
                "_score": 0.46000168,
                "entrezgene": 34411,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            983
                        ],
                        [
                            10090,
                            12534
                        ],
                        [
                            10116,
                            54237
                        ],
                        [
                            7227,
                            34411
                        ],
                        [
                            6239,
                            176374
                        ],
                        [
                            7955,
                            80973
                        ],
                        [
                            8364,
                            394503
                        ]
                    ],
                    "id": 68203
                },
                "id": "34411",
                "name": "Cyclin-dependent kinase 1",
                "symbol": "Cdk1",
                "taxid": 7227
            },
            {
                "_id": "36854",
                "_score": 0.46000168,
                "entrezgene": 36854,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1021
                        ],
                        [
                            10090,
                            12571
                        ],
                        [
                            10116,
                            114483
                        ],
                        [
                            7227,
                            36854
                        ],
                        [
                            6239,
                            181472
                        ],
                        [
                            7955,
                            100034507
                        ],
                        [
                            8364,
                            100489734
                        ]
                    ],
                    "id": 963
                },
                "id": "36854",
                "name": "Cyclin-dependent kinase 4",
                "symbol": "Cdk4",
                "taxid": 7227
            },
            {
                "_id": "1025",
                "_score": 0.427744,
                "entrezgene": 1025,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1025
                        ],
                        [
                            10090,
                            107951
                        ],
                        [
                            10116,
                            362110
                        ],
                        [
                            7227,
                            37586
                        ],
                        [
                            6239,
                            173023
                        ],
                        [
                            7955,
                            321602
                        ],
                        [
                            8364,
                            448039
                        ]
                    ],
                    "id": 55566
                },
                "id": "1025",
                "name": "cyclin dependent kinase 9",
                "symbol": "CDK9",
                "taxid": 9606
            },
            {
                "_id": "1024",
                "_score": 0.4130674,
                "entrezgene": 1024,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1024
                        ],
                        [
                            10090,
                            264064
                        ],
                        [
                            10116,
                            498140
                        ],
                        [
                            7227,
                            39157
                        ],
                        [
                            6239,
                            172677
                        ],
                        [
                            7955,
                            373868
                        ],
                        [
                            8364,
                            394856
                        ]
                    ],
                    "id": 55565
                },
                "id": "1024",
                "name": "cyclin dependent kinase 8",
                "symbol": "CDK8",
                "taxid": 9606
            },
            {
                "_id": "361434",
                "_score": 0.39537928,
                "entrezgene": 361434,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            8558
                        ],
                        [
                            10090,
                            234854
                        ],
                        [
                            10116,
                            361434
                        ],
                        [
                            7227,
                            36051
                        ],
                        [
                            7955,
                            550285
                        ],
                        [
                            8364,
                            549329
                        ]
                    ],
                    "id": 55769
                },
                "id": "361434",
                "name": "cyclin-dependent kinase 10",
                "symbol": "Cdk10",
                "taxid": 10116
            },
            {
                "_id": "10983",
                "_score": 0.3939471,
                "entrezgene": 10983,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            10983
                        ],
                        [
                            10090,
                            12453
                        ],
                        [
                            10116,
                            289500
                        ],
                        [
                            7955,
                            406239
                        ],
                        [
                            8364,
                            448195
                        ]
                    ],
                    "id": 4979
                },
                "id": "10983",
                "name": "cyclin I",
                "symbol": "CCNI",
                "taxid": 9606
            },
            {
                "_id": "901",
                "_score": 0.38944373,
                "entrezgene": 901,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            901
                        ],
                        [
                            10090,
                            12452
                        ],
                        [
                            10116,
                            29157
                        ],
                        [
                            7955,
                            266794
                        ],
                        [
                            8364,
                            549201
                        ]
                    ],
                    "id": 3208
                },
                "id": "901",
                "name": "cyclin G2",
                "symbol": "CCNG2",
                "taxid": 9606
            },
            {
                "_id": "114839",
                "_score": 0.37950137,
                "entrezgene": 114839,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            892
                        ],
                        [
                            10090,
                            51813
                        ],
                        [
                            10116,
                            114839
                        ],
                        [
                            7227,
                            41801
                        ],
                        [
                            6239,
                            175357
                        ],
                        [
                            7955,
                            335429
                        ],
                        [
                            3702,
                            834920
                        ],
                        [
                            3702,
                            834921
                        ],
                        [
                            8364,
                            394762
                        ]
                    ],
                    "id": 3803
                },
                "id": "114839",
                "name": "cyclin C",
                "symbol": "Ccnc",
                "taxid": 10116
            },
            {
                "_id": "8558",
                "_score": 0.3781267,
                "entrezgene": 8558,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            8558
                        ],
                        [
                            10090,
                            234854
                        ],
                        [
                            10116,
                            361434
                        ],
                        [
                            7227,
                            36051
                        ],
                        [
                            7955,
                            550285
                        ],
                        [
                            8364,
                            549329
                        ]
                    ],
                    "id": 55769
                },
                "id": "8558",
                "name": "cyclin dependent kinase 10",
                "symbol": "CDK10",
                "taxid": 9606
            },
            {
                "_id": "51265",
                "_score": 0.3781267,
                "entrezgene": 51265,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            51265
                        ],
                        [
                            10090,
                            213084
                        ],
                        [
                            10116,
                            60396
                        ]
                    ],
                    "id": 49467
                },
                "id": "51265",
                "name": "cyclin dependent kinase like 3",
                "symbol": "CDKL3",
                "taxid": 9606
            },
            {
                "_id": "900",
                "_score": 0.35650128,
                "entrezgene": 900,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            900
                        ],
                        [
                            10090,
                            12450
                        ],
                        [
                            10116,
                            25405
                        ],
                        [
                            7227,
                            43724
                        ],
                        [
                            7955,
                            171473
                        ],
                        [
                            8364,
                            448326
                        ]
                    ],
                    "id": 2995
                },
                "id": "900",
                "name": "cyclin G1",
                "symbol": "CCNG1",
                "taxid": 9606
            },
            {
                "_id": "8900",
                "_score": 0.35287082,
                "entrezgene": 8900,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            8900
                        ],
                        [
                            10090,
                            12427
                        ],
                        [
                            10116,
                            295052
                        ],
                        [
                            7955,
                            404206
                        ],
                        [
                            3702,
                            834324
                        ],
                        [
                            3702,
                            841124
                        ],
                        [
                            3702,
                            841125
                        ],
                        [
                            3702,
                            841126
                        ],
                        [
                            8364,
                            548993
                        ]
                    ],
                    "id": 31203
                },
                "id": "8900",
                "name": "cyclin A1",
                "symbol": "CCNA1",
                "taxid": 9606
            },
            {
                "_id": "1031",
                "_score": 0.35287082,
                "entrezgene": 1031,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1031
                        ],
                        [
                            10090,
                            12580
                        ],
                        [
                            10116,
                            54238
                        ],
                        [
                            7955,
                            555797
                        ],
                        [
                            8364,
                            100144705
                        ]
                    ],
                    "id": 966
                },
                "id": "1031",
                "name": "cyclin dependent kinase inhibitor 2C",
                "symbol": "CDKN2C",
                "taxid": 9606
            },
            {
                "_id": "466",
                "_score": 0.3447037,
                "entrezgene": 466,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            466
                        ],
                        [
                            10090,
                            11908
                        ],
                        [
                            10090,
                            100040260
                        ],
                        [
                            10116,
                            315305
                        ],
                        [
                            7955,
                            325609
                        ],
                        [
                            8364,
                            448303
                        ]
                    ],
                    "id": 3790
                },
                "id": "466",
                "name": "activating transcription factor 1",
                "symbol": "ATF1",
                "taxid": 9606
            },
            {
                "_id": "1028",
                "_score": 0.34076324,
                "entrezgene": 1028,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1028
                        ]
                    ],
                    "id": 133549
                },
                "id": "1028",
                "name": "cyclin dependent kinase inhibitor 1C",
                "symbol": "CDKN1C",
                "taxid": 9606
            },
            {
                "_id": "894",
                "_score": 0.34076324,
                "entrezgene": 894,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            894
                        ],
                        [
                            10090,
                            12444
                        ],
                        [
                            10116,
                            64033
                        ],
                        [
                            7227,
                            32551
                        ],
                        [
                            7955,
                            799608
                        ],
                        [
                            8364,
                            100038071
                        ]
                    ],
                    "id": 37525
                },
                "id": "894",
                "name": "cyclin D2",
                "symbol": "CCND2",
                "taxid": 9606
            },
            {
                "_id": "12580",
                "_score": 0.3301685,
                "entrezgene": 12580,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1031
                        ],
                        [
                            10090,
                            12580
                        ],
                        [
                            10116,
                            54238
                        ],
                        [
                            7955,
                            555797
                        ],
                        [
                            8364,
                            100144705
                        ]
                    ],
                    "id": 966
                },
                "id": "12580",
                "name": "cyclin-dependent kinase inhibitor 2C (p18, inhibits CDK4)",
                "symbol": "Cdkn2c",
                "taxid": 10090
            },
            {
                "_id": "2967",
                "_score": 0.3119386,
                "entrezgene": 2967,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            2967
                        ],
                        [
                            10090,
                            209357
                        ],
                        [
                            10116,
                            288651
                        ],
                        [
                            7227,
                            33294
                        ],
                        [
                            7955,
                            436837
                        ],
                        [
                            3702,
                            838415
                        ],
                        [
                            8364,
                            100145792
                        ]
                    ],
                    "id": 1160
                },
                "id": "2967",
                "name": "general transcription factor IIH subunit 3",
                "symbol": "GTF2H3",
                "taxid": 9606
            },
            {
                "_id": "898",
                "_score": 0.3119386,
                "entrezgene": 898,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            898
                        ],
                        [
                            10090,
                            12447
                        ],
                        [
                            10116,
                            25729
                        ],
                        [
                            7227,
                            34924
                        ],
                        [
                            7955,
                            30188
                        ],
                        [
                            8364,
                            549082
                        ]
                    ],
                    "id": 14452
                },
                "id": "898",
                "name": "cyclin E1",
                "symbol": "CCNE1",
                "taxid": 9606
            },
            {
                "_id": "905",
                "_score": 0.3024607,
                "entrezgene": 905,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            905
                        ],
                        [
                            10090,
                            72949
                        ],
                        [
                            10116,
                            304758
                        ],
                        [
                            7955,
                            327568
                        ],
                        [
                            8364,
                            100488125
                        ]
                    ],
                    "id": 14043
                },
                "id": "905",
                "name": "cyclin T2",
                "symbol": "CCNT2",
                "taxid": 9606
            },
            {
                "_id": "2580",
                "_score": 0.3024607,
                "entrezgene": 2580,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            2580
                        ],
                        [
                            10090,
                            231580
                        ],
                        [
                            10116,
                            81659
                        ],
                        [
                            7227,
                            40527
                        ],
                        [
                            7955,
                            100151158
                        ],
                        [
                            8364,
                            549104
                        ]
                    ],
                    "id": 3846
                },
                "id": "2580",
                "name": "cyclin G associated kinase",
                "symbol": "GAK",
                "taxid": 9606
            },
            {
                "_id": "8899",
                "_score": 0.29524684,
                "entrezgene": 8899,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            8899
                        ],
                        [
                            10090,
                            19134
                        ],
                        [
                            10116,
                            291078
                        ]
                    ],
                    "id": 134085
                },
                "id": "8899",
                "name": "pre-mRNA processing factor 4B",
                "symbol": "PRPF4B",
                "taxid": 9606
            },
            {
                "_id": "896",
                "_score": 0.29524684,
                "entrezgene": 896,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            896
                        ],
                        [
                            10090,
                            12445
                        ],
                        [
                            10116,
                            25193
                        ]
                    ],
                    "id": 20419
                },
                "id": "896",
                "name": "cyclin D3",
                "symbol": "CCND3",
                "taxid": 9606
            },
            {
                "_id": "1019",
                "_score": 0.29524684,
                "entrezgene": 1019,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1019
                        ],
                        [
                            10090,
                            12567
                        ],
                        [
                            10116,
                            94201
                        ],
                        [
                            7955,
                            777730
                        ],
                        [
                            8364,
                            549496
                        ]
                    ],
                    "id": 55429
                },
                "id": "1019",
                "name": "cyclin dependent kinase 4",
                "symbol": "CDK4",
                "taxid": 9606
            },
            {
                "_id": "595",
                "_score": 0.26737595,
                "entrezgene": 595,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            595
                        ],
                        [
                            10090,
                            12443
                        ],
                        [
                            10116,
                            58919
                        ],
                        [
                            7955,
                            30222
                        ],
                        [
                            8364,
                            448045
                        ]
                    ],
                    "id": 1334
                },
                "id": "595",
                "name": "cyclin D1",
                "symbol": "CCND1",
                "taxid": 9606
            },
            {
                "_id": "83571",
                "_score": 0.2444517,
                "entrezgene": 83571,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1027
                        ],
                        [
                            10090,
                            12576
                        ],
                        [
                            10116,
                            83571
                        ],
                        [
                            7955,
                            368329
                        ]
                    ],
                    "id": 2999
                },
                "id": "83571",
                "name": "cyclin-dependent kinase inhibitor 1B",
                "symbol": "Cdkn1b",
                "taxid": 10116
            },
            {
                "_id": "1029",
                "_score": 0.24340233,
                "entrezgene": 1029,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1029
                        ],
                        [
                            10090,
                            12578
                        ],
                        [
                            10116,
                            25163
                        ],
                        [
                            7955,
                            100329528
                        ],
                        [
                            8364,
                            448767
                        ]
                    ],
                    "id": 55430
                },
                "id": "1029",
                "name": "cyclin dependent kinase inhibitor 2A",
                "symbol": "CDKN2A",
                "taxid": 9606
            },
            {
                "_id": "114851",
                "_score": 0.22137581,
                "entrezgene": 114851,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1026
                        ],
                        [
                            10090,
                            12575
                        ],
                        [
                            10116,
                            114851
                        ]
                    ],
                    "id": 333
                },
                "id": "114851",
                "name": "cyclin-dependent kinase inhibitor 1A",
                "symbol": "Cdkn1a",
                "taxid": 10116
            },
            {
                "_id": "1017",
                "_score": 0.19683123,
                "entrezgene": 1017,
                "homologene": {
                    "genes": [
                        [
                            9606,
                            1017
                        ],
                        [
                            10090,
                            12566
                        ],
                        [
                            10116,
                            362817
                        ],
                        [
                            7227,
                            42453
                        ],
                        [
                            7955,
                            406715
                        ],
                        [
                            3702,
                            824036
                        ],
                        [
                            8364,
                            493498
                        ]
                    ],
                    "id": 74409
                },
                "id": "1017",
                "name": "cyclin dependent kinase 2",
                "symbol": "CDK2",
                "taxid": 9606
            }
        ],
        "qtype": "keyword",
        "query": "CDK",
        "totalCount": 81
    },
    "success": true
};

        return { "tweets": data, "friends": friends };

    }
}
