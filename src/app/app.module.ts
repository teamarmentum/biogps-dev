import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { UserService } from './user.service';
import {
    routing,
    appRoutingProviders
} from './app.routing';
import { InMemoryWebApiModule } from 'angular2-in-memory-web-api';
import { MockDatabaseService } from './mock.database.service';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SignupComponent } from './signup/signup.component';
import { HelpComponent } from './help/help.component';
import { AboutComponent } from './about/about.component';
import { BlogComponent } from './blog/blog.component';
import { ContactComponent } from './contact/contact.component';
import { SigninComponent } from './signin/signin.component';
import { HttpGeneService } from './dashboard/dashboard.service';
import { Ng2TableModule} from 'ng2-table';
import { Ng2BootstrapModule } from 'ng2-bootstrap'


@NgModule({
    declarations: [
        AppComponent,
        MenuComponent,
        HomeComponent,
        SignupComponent,
        HelpComponent,
        AboutComponent,
        BlogComponent,
        ContactComponent,
        SigninComponent,
        DashboardComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        routing,
        Ng2TableModule,
        Ng2BootstrapModule,
    ],
    providers: [UserService, appRoutingProviders, HttpGeneService],
    bootstrap: [AppComponent]
})
export class AppModule { }
