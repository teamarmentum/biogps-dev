export class Tweet {

    public avatar;

    constructor(
        public id: number,
        public body: string,
        public author: string,
        public date: Date,
        public retweets: Array<string>,
        public favourites: Array<string>){
        this.avatar = `${author.toLowerCase()}.jpg`;

    }

    hasFavourited(userId : string) : boolean {
        return this.favourites.indexOf(userId) != -1;
    }

    hasRetweeted(userId : string) : boolean {
        return this.retweets.indexOf(userId) != -1;
    }
}
