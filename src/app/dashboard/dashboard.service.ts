import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';
import {Observable}     from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpGeneService {
    private _genesUrl:string = "/boe/?query=";
    constructor(private _http: Http){ }

    getGenesRestful(query: string): Observable<any>{
        return this._http.get(this._genesUrl+query).map((resp: Response) => {
            return resp.json();
        });
    }
    private handleError (error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || ' error');
    }
}
