import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpGeneService } from './dashboard.service';
import { Subscription } from 'rxjs';

declare var $: any;

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: [
        './dashboard.component.css',
        './dashboard.css',
        './media_dashboard.css',
        './scroll.css',
        './font-awesome.min.css',
        './pe-icon-7-stroke.css',
        './helper.css',
        './bootstrap-multiselect.css',
        './dataTables.bootstrap.min.css',
        './gridstack.css',
    ]
})


export class DashboardComponent implements OnInit {

    public rows: Array<any> = [];
    public columns: Array<any> = [
        { title: 'Symbol', name: 'symbol' },
        { title: 'Id', name: 'id' },
        { title: 'Name.', name: 'name' },
        { title: 'Species', name: 'taxid' },
    ];

    selectedSpecies: Array<any> = [];
    currentGeneList: Array<any> = [];
    public query: any;
    public page: number = 1;
    public itemsPerPage: number = 10;
    public maxSize: number = 5;
    public numPages: number = 1;
    public length: number = 0;
    public species: Array<any> = [];
    public showSpecies = false;

    public config: any = {
        paging: true,
        sorting: { columns: this.columns },
        filtering: { filterString: '' },
        className: ['table-striped', 'table-bordered']
    };
    private subscription: Subscription;
    private data: any;
    displayProfile = ""
    private species_d: any = {
        '9606': 'human',
        '10090': 'mouse',
        '10116': 'rat',
        '7227': 'fruitfly',
        '6239': 'nematode',
        '7955': 'zebrafish',
        '3702': 'thale-cress',
        '8364': 'frog',
        '9823': 'pig'
    }

    public constructor(
        private _httpGeneService: HttpGeneService,
        private activatedRoute: ActivatedRoute) {
        this.length = 0;
        this.subscription = this.activatedRoute.params.subscribe(
            (param: any) => {
                this.query = param['query'] || "";
            });
    }

    public ngOnInit(): void {

        this._httpGeneService.getGenesRestful(this.query)
            .subscribe(
            data => this.parseGene(data),
            error => console.log("Error HTTP GET Service"),
        );
    }
    parseGene(genes) {
        this.data = genes.data.data.geneList;
        this.length = this.data.length;
        this.onChangeTable(this.config);
    }

    public changePage(page: any, data: Array<any> = this.data): Array<any> {
        let start = (page.page - 1) * page.itemsPerPage;
        let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }

    public changeSort(data: any, config: any): any {
        if (!config.sorting) {
            return data;
        }

        let columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;

        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '' && columns[i].sort !== false) {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }

        if (!columnName) {
            return data;
        }

        // simple sorting
        return data.sort((previous: any, current: any) => {
            if (previous[columnName] > current[columnName]) {
                return sort === 'desc' ? -1 : 1;
            } else if (previous[columnName] < current[columnName]) {
                return sort === 'asc' ? -1 : 1;
            }
            return 0;
        });
    }

    public changeFilter(data: any, config: any): any {
        let filteredData: Array<any> = data;
        this.columns.forEach((column: any) => {
            if (column.filtering) {
                filteredData = filteredData.filter((item: any) => {
                    return item[column.name].match(column.filtering.filterString);
                });
            }
        });

        if (!config.filtering) {
            return filteredData;
        }

        if (config.filtering.columnName) {
            return filteredData.filter((item: any) =>
                item[config.filtering.columnName].match(this.config.filtering.filterString));
        }

        let tempArray: Array<any> = [];
        filteredData.forEach((item: any) => {
            let flag = false;
            this.columns.forEach((column: any) => {
                var flagData = item[column.name] || ""
                if (column.name == "taxid") {
                    var species = this.species_d[flagData] || flagData;
                    if (species) {
                        if (this.species.indexOf(species) == -1) {
                            this.species.push(species);
                        }
                    }
                    item[column.name] = species;
                } else {
                }
                if (flagData.toString().match(this.config.filtering.filterString)) {
                    flag = true;
                }
            });
            if (flag) {
                tempArray.push(item);
            }
        });
        filteredData = tempArray;

        return filteredData;
    }

    public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        if (config.filtering) {
            Object.assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
            Object.assign(this.config.sorting, config.sorting);
        }

        let filteredData = this.changeFilter(this.data, this.config);
        let sortedData = this.changeSort(filteredData, this.config);
        this.addCurrentGeneList(sortedData);
        this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
        this.length = sortedData.length;
    }

    public onCellClick(data: any): any {
        console.log(data);
    }
    ngOnDestroy() {
        // prevent memory leak by unsubscribing
        this.subscription.unsubscribe();
    }

    public getStyle() {
        if (this.showSpecies) {
            return "block";
        } else {
            return "none";
        }
    }
    public addCurrentGeneList(data) {
        var clist = [];
        data.forEach( function (d) {
            if(d && clist.indexOf(d.symbol) == -1){
                clist.push(d.symbol);
            }
        });
        this.currentGeneList = clist;
    }
    public changeCheckBox(event) {
        var target = event.target || event.srcElement || event.currentTarget;
        var data = target.attributes.value.value || "";
        if (target.checked){
            if (this.selectedSpecies.indexOf(data) == -1){
                if (data == "all")
                    this.selectedSpecies = [];
                else
                    this.selectedSpecies.push(data);
            }

        } else {
            if (data == "all")
                    this.selectedSpecies = [];
            else {
                var index = this.selectedSpecies.indexOf(data);
                if(index != -1)
                this.selectedSpecies.splice( index, 1 );
            }
        }
        if (data.toLowerCase() == 'all'){
            $('input.checkbox-species').each(function(index, obj){
                obj.checked = target.checked;
            });
        }
        this.config.filtering = {filterString: this.selectedSpecies.join("|")}
        this.onChangeTable(this.config);
    }
    slideDropDown(event) {
        if (this.displayProfile == "none") {
            this.displayProfile = "block"
         } else {
            this.displayProfile = "none"
        }
    }

    slideToggle() {
        $('.dashboard-top-right ul').slideToggle();
    }
}
