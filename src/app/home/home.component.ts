import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

    geneText = '';
    urlText = ''
    constructor(private router: Router) { }

    ngOnInit() {
    }

    pushGene(gene) {
        this.geneText = gene;
        this.urlText = encodeURIComponent(gene);
    }

    SearchGene(event){
        event.preventDefault();
        this.router.navigate(['/dashboard', this.geneText]);
    }

}
